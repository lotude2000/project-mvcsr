<?php

namespace App\Repositories;

use App\Models\Category;

class CategoryRepository extends BaseRepository
{
    public function model()
    {
        return Category::class;
    }

    public function getParent()
    {
        return $this->model->where('parent_id', 0)->get();
    }

    public function search($dataSearch)
    {
        return $this->model
            ->withName($dataSearch['name'])
            ->WithParentId($dataSearch['parent_id'])
            ->latest('id')->paginate(5);
    }
}
