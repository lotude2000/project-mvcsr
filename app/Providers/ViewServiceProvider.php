<?php

namespace App\Providers;

use App\Http\Composers\CategoryComposer;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer([
            'index','categories.index','categories.create','products.edit',
            'categories.edit','products.create','products.index'
        ],CategoryComposer::class);
    }
}
