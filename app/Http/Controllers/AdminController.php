<?php

namespace App\Http\Controllers;

class AdminController extends Controller
{
    public function index()
    {
        $user = Auth()->user();
        return view('index', compact('user'));
    }
}
