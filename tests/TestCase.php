<?php

namespace Tests;

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\WithFaker;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    use WithFaker;

    public function loginUserWithPermission($permission)
    {
        $user = User::factory()->create();
        $role = Role::factory()->create();
        $user->roles()->attach(Role::all()->pluck('id'));
        $permissions = Permission::where('name', $permission)->first();
        $role->permissions()->attach($permissions->pluck('id'));
        return $this->actingAs($user);
    }

    public function loginWithSuperAdmin()
    {
        $user = User::factory()->create();
        $role = Role::where('name', 'super_admin')->pluck('id');
        $user->roles()->attach($role);
        return $this->actingAs($user);
    }

    public function loginWithUser()
    {
        $user = User::factory()->create();
        $role = Role::where('name', 'customer')->pluck('id');
        $user->roles()->attach($role);
        return $this->actingAs($user);
    }
}
