<?php

namespace Tests\Feature\Products;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;

class CreateNewProductTest extends TestCase
{

    public function getStoreProductRoute()
    {
        return route('products.store');
    }

    public function getCreateProductRoute()
    {
        return route('products.create');
    }

    public function makeFactoryProduct()
    {
        return Product::factory()->make()->toArray();
    }

    /** @test */
    public function authenticated_super_admin_can_view_create_product_form_if_product_is_exits()
    {
        $this->loginWithSuperAdmin();
        $response = $this->get($this->getCreateProductRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('products.create');
    }

    /** @test */
    public function authenticated_authorize_user_can_view_create_product_form_if_product_is_exits()
    {
        $this->loginUserWithPermission('product_create');
        $response = $this->get($this->getCreateProductRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('products.create');
    }

    /** @test */
    public function authenticated_not_authorize_user_can_not_view_create_product_form_if_product_is_exits()
    {
        $this->loginWithUser();
        $response = $this->get($this->getCreateProductRoute());
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function unauthenticated_user_can_not_view_create_product_form_if_product_is_exits()
    {
        $response = $this->get($this->getCreateProductRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('login');
    }

    /** @test */
    public function authenticated_super_admin_can_create_new_product_if_product_is_exits()
    {
        $this->loginWithSuperAdmin();
        $dataCreate = $this->makeFactoryProduct();
        $response = $this->post($this->getStoreProductRoute(), $dataCreate);
        $dataCreate['image'] = 'default.png';
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('products',$dataCreate);
        $response->assertRedirect(route('products.index'));
    }

    /** @test */
    public function authenticated_super_admin_can_not_create_new_product_if_name_field_is_null()
    {
        $this->loginWithSuperAdmin();
        $dataCreate = Product::factory()->make([
            'name'=>''
        ])->toArray();
        $response = $this->post($this->getStoreProductRoute(), $dataCreate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function authenticated_authorize_user_can_not_create_new_product_if_product_field_is_not_validate()
    {
        $this->loginUserWithPermission('product_store');
        $dataCreate = Product::factory()->make(['price' => null])->toArray();
        $response = $this->post($this->getStoreProductRoute(), $dataCreate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['price']);
    }

    /** @test */
    public function unauthenticated_user_can_not_create_new_product_if_product_exits()
    {
        $dataCreate = $this->makeFactoryProduct();
        $response = $this->post($this->getStoreProductRoute(), $dataCreate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function authenticated_not_authorize_user_can_not_create_new_product_if_product_exits()
    {
        $this->loginWithUser();
        $dataCreate = $this->makeFactoryProduct();
        $response = $this->post($this->getStoreProductRoute(), $dataCreate);
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function authenticated_authorize_user_can_not_create_new_product_if_product_data_is_not_validate()
    {
        $this->loginUserWithPermission('product_store');
        $dataCreate = Product::factory()->make([
            'name'=>'',
            'price' => null,
            'description'=>''
        ])->toArray();
        $response = $this->post($this->getStoreProductRoute(), $dataCreate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name','price','description']);
    }


}
