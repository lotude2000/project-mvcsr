<?php

namespace Tests\Feature\Products;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class UpdateProductTest extends TestCase
{

    public function getUpdateProductRoute($id)
    {
        return route('products.update',$id);
    }

    public function getEditProductRoute($id)
    {
        return route('products.edit',$id);
    }

    public function makeFactoryProduct()
    {
        return Product::factory()->make()->toArray();
    }

    public function createFactoryProduct()
    {
        return Product::factory()->create();
    }

    /** @test  */
    public function authenticated_super_admin_can_view_edit_product_form_if_product_is_exits()
    {
        $this->loginWithSuperAdmin();
        $product = $this->createFactoryProduct();
        $response = $this->get($this->getEditProductRoute($product->id));
        $response->assertStatus(Response::HTTP_OK);
       $response->assertViewIs('products.edit');
    }

    /** @test  */
    public function authenticated_authorize_user_can_view_edit_product_form_if_product_is_exits()
    {
        $this->loginUserWithPermission('product_edit');
        $product = $this->createFactoryProduct();
        $response = $this->get($this->getEditProductRoute($product->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('products.edit');
    }

    /** @test  */
    public function authenticated_not_authorize_user_can_not_view_edit_product_form_if_product_is_exits()
    {
        $this->loginWithUser();
        $product = $this->createFactoryProduct();
        $response = $this->get($this->getEditProductRoute($product->id));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test  */
    public function unauthenticated__user_can_not_view_edit_product_form_if_product_is_exits()
    {
        $product = $this->createFactoryProduct();
        $response = $this->get($this->getEditProductRoute($product->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('login');
    }

    /** @test  */
    public function authenticated_super_admin_can_update_product_if_product_is_exits()
    {
        $this->loginWithSuperAdmin();
        $product = $this->createFactoryProduct();
        $category = Category::factory()->create();
        $product->categories()->attach($category->pluck('id'));
        $dataUpdate = $this->makeFactoryProduct();
        $categoryUpdate = Category::factory()->create();
        $product->categories()->sync($categoryUpdate->pluck('id'));
        $response = $this->patch($this->getUpdateProductRoute($product->id), $dataUpdate);
        $this->assertDatabaseHas('products',$dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('products.index'));
    }

    /** @test  */
    public function authenticated_authorize_user_can_update_product_if_product_is_exits()
    {
        $this->loginUserWithPermission('product_update');
        $product = $this->createFactoryProduct();
        $category = Category::factory()->create();
        $product->categories()->attach($category->pluck('id'));
        $dataUpdate = $this->makeFactoryProduct();
        $categoryUpdate = Category::factory()->create();
        $product->categories()->sync($categoryUpdate->pluck('id'));
        $response = $this->patch($this->getUpdateProductRoute($product->id), $dataUpdate);
        $this->assertDatabaseHas('products',$dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('products.index'));
    }

    /** @test  */
    public function authenticated_not_authorize_user_can_not_update_product_if_product_is_exits()
    {
        $this->loginWithUser();
        $product = $this->createFactoryProduct();
        $dataUpdate = $this->makeFactoryProduct();
        $response = $this->patch($this->getUpdateProductRoute($product->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test  */
    public function authenticated_authorize_user_can_not_update_product_if_product_name_is_not_validate()
    {
        $this->loginUserWithPermission('product_update');
        $product = $this->createFactoryProduct();
        $dataUpdate = Product::factory()->make([
            'name'=>''
        ])->toArray();
        $response = $this->patch($this->getUpdateProductRoute($product->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }

    /** @test  */
    public function authenticated_authorize_user_can_not_update_product_if_product_price_is_not_validate()
    {
        $this->loginUserWithPermission('product_update');
        $product = $this->createFactoryProduct();
        $dataUpdate = Product::factory()->make([
            'price'=>''
        ])->toArray();
        $response = $this->patch($this->getUpdateProductRoute($product->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['price']);
    }

    /** @test  */
    public function authenticated_authorize_user_can_not_update_product_if_product_description_is_not_validate()
    {
        $this->loginUserWithPermission('product_update');
        $product = $this->createFactoryProduct();
        $dataUpdate = Product::factory()->make([
            'description'=>''
        ])->toArray();
        $response = $this->patch($this->getUpdateProductRoute($product->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['description']);
    }

    /** @test */
    public function authenticated_authorize_user_can_not_update_product_if_product_data_is_not_validate()
    {
        $this->loginUserWithPermission('product_update');
        $product = $this->createFactoryProduct();
        $dataUpdate = Product::factory()->make([
            'name'=>'',
            'price' => null,
            'description'=>''
        ])->toArray();
        $response = $this->patch($this->getUpdateProductRoute($product->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name','price','description']);
    }
}
