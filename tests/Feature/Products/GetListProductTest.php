<?php

namespace Tests\Feature\Products;

use App\Models\Category;
use App\Models\Product;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetListProductTest extends TestCase
{

    public function getListProductRoute($searchField)
    {
        return route('products.index',$searchField);
    }

    /** @test */
    public function authenticated_super_admin_can_see_all_product_if_product_exits()
    {
        $this->loginWithSuperAdmin();
        $products = Product::factory()->create();
        $response = $this->get($this->getListProductRoute(''));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('products.index');
        $response->assertSee($products->name);
    }

    /** @test */
    public function authenticated_authorize_user_can_see_all_product_if_product_exits()
    {
        $this->loginUserWithPermission('product_list');
        $products = Product::factory()->create();
        $response = $this->get($this->getListProductRoute(''));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('products.index');
        $response->assertSee($products->name);
    }

    /** @test */
    public function unauthenticated_user_can_not_see_all_product_if_product_exits()
    {
        $response = $this->get($this->getListProductRoute(''));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function authenticated_not_authorize_user_can_not_see_all_product_if_product_exits()
    {
        $this->loginWithUser();
        $response = $this->get($this->getListProductRoute(''));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function authenticated_super_admin_can_search_product_with_product_name_if_product_exits()
    {
        $this->loginWithSuperAdmin();
        $products = Product::factory()->create();
        $response = $this->get($this->getListProductRoute($products->name));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('products.index');
        $response->assertSee($products->name);
    }

    /** @test */
    public function authenticated_super_admin_can_search_product_with_product_price_if_product_exits()
    {
        $this->loginWithSuperAdmin();
        $products = Product::factory()->create();
        $response = $this->get($this->getListProductRoute($products->price));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('products.index');
        $response->assertSee(number_format($products->price));
    }

    /** @test */
    public function authenticated_super_admin_can_search_product_with_category_id_if_product_exits()
    {
        $this->loginWithSuperAdmin();
        $products = Product::factory()->create();
        $category = Category::factory()->create();
        $products->categories()->attach($category->id);
        $response = $this->get($this->getListProductRoute($category->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('products.index');
        $response->assertSee($products->name);
    }

    /** @test */
    public function authenticated_super_admin_can_search_product_with_name_price_and_category_id_if_product_exits()
    {
        $this->loginWithSuperAdmin();
        $products = Product::factory()->create();
        $category = Category::factory()->create();
        $products->categories()->attach($category->id);
        $response = $this->get($this->getListProductRoute(
            [
                $products->name,
                $products->price,
                $category->id
            ]
        ));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('products.index');
        $response->assertSee($products->name,$products->price);
    }
}
