<?php

namespace Tests\Feature\Products;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class ShowProductTest extends TestCase
{
    public function getShowProductRoute($id)
    {
        return route('products.show', $id);
    }

    public function createFactoryProduct()
    {
        return Product::factory()->create();
    }

    public function createFactoryCategory()
    {
        return Category::factory()->create();
    }

    /** @test */
    public function authenticated_super_admin_can_get_product_if_product_exits()
    {
        $this->loginWithSuperAdmin();
        $product = $this->createFactoryProduct();
        $category = $this->createFactoryCategory();
        $product->categories()->attach($category->pluck('id'));
        $response = $this->get($this->getShowProductRoute($product->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('products.show');
        $response->assertSee($product->name);
    }

    /** @test */
    public function authenticated_authenticated_user_can_get_product_if_product_exits()
    {
        $this->loginUserWithPermission('product_show');
        $product = $this->createFactoryProduct();
        $category = $this->createFactoryCategory();
        $product->categories()->attach($category->pluck('id'));
        $response = $this->get($this->getShowProductRoute($product->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('products.show');
        $response->assertSee($product->name);
    }

    /** @test */
    public function authenticated_not_authenticated_user_can_not_get_product_if_product_exits()
    {
        $this->loginWithUser();
        $product = $this->createFactoryProduct();
        $category = $this->createFactoryCategory();
        $product->categories()->attach($category->pluck('id'));
        $response = $this->get($this->getShowProductRoute($product->id));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function unauthenticated_user_can_not_get_product_if_product_exits()
    {
        $product = $this->createFactoryProduct();
        $response = $this->get($this->getShowProductRoute($product->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function authenticated_authenticated_user_can_not_get_product_if_product_id_is_not_exist()
    {
        $this->loginUserWithPermission('product_show');
        $productId = -1;
        $response = $this->get($this->getShowProductRoute($productId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }


}
