<?php

namespace Tests\Feature\Products;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteProductTest extends TestCase
{

    public function getDeleteProductRoute($id)
    {
        return route('products.destroy',$id);
    }

    public function createFactoryProduct()
    {
        return Product::factory()->create();
    }

    /** @test  */
    public function authenticated_super_admin_can_delete_product_if_product_is_exits()
    {
        $this->loginWithSuperAdmin();
        $product = $this->createFactoryProduct();
        $category = Category::factory()->create();
        $product->categories()->attach($category->id);
        $response = $this->delete($this->getDeleteProductRoute($product->id));
        $product->categories()->detach($category->id);
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseMissing('products',$product->toArray());
    }

    /** @test  */
    public function authenticated_authorize_user_can_delete_product_if_product_is_exits()
    {
        $this->loginUserWithPermission('product_delete');
        $product = $this->createFactoryProduct();
        $category = Category::factory()->create();
        $product->categories()->attach($category->id);
        $response = $this->delete($this->getDeleteProductRoute($product->id));
        $product->categories()->detach($category->id);
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseMissing('products',$product->toArray());
    }

    /** @test  */
    public function authenticated_not_authorize_user_can_not_delete_product_if_product_is_exits()
    {
        $this->loginWithUser();
        $product = $this->createFactoryProduct();
        $response = $this->delete($this->getDeleteProductRoute($product->id));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test  */
    public function unauthenticated_user_can_not_delete_product_if_product_is_exits()
    {
        $product = $this->createFactoryProduct();
        $response = $this->delete($this->getDeleteProductRoute($product->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('login');
    }

    /** @test  */
    public function authenticated_authorize_user_can_not_delete_product_if_product_id_not_exits()
    {
        $this->loginUserWithPermission('product_delete');
        $productId = -1;
        $response = $this->delete($this->getDeleteProductRoute($productId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
