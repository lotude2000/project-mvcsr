<?php

namespace Tests\Feature\Auth;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class LoginTest extends TestCase
{
    /** @test */
    public function user_can_view_login_form_if_login_exits()
    {
        $response = $this->get('/login');
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('auth.login')->assertSee('Login');
    }

    /** @test  */
    public function user_can_login_if_login_exits()
    {
        $this->assertGuest();
        $user = User::factory()->create([
            'password' => bcrypt('12345678'),
        ]);

        $response = $this->post('/login', [
            'email' => $user->email,
            'password' => '12345678',
        ]);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/home');
        $this->assertAuthenticatedAs($user);
    }

    /** @test  */
    public function user_can_not_login_if_incorrect_password()
    {
        $user = User::factory()->make([
            'password' => bcrypt('12345678'),
        ]);

        $response = $this->from('/login')->post('/login', [
            'email' => $user->email,
            'password' => '12345678',
        ]);

        $response->assertRedirect('/login');
        $response->assertSessionHasErrors('email');
        $this->assertTrue(session()->hasOldInput('email'));
        $this->assertFalse(session()->hasOldInput('password'));
        $this->assertGuest();
    }

    /** @test  */
    public function user_can_not_login_if_email_is_not_exits()
    {
        $user = User::factory()->make([
            'password' => bcrypt('12345678'),
        ]);

        $response = $this->from('/login')->post('/login', [
            'email' => 'abc',
            'password' => '12345678',
        ]);

        $response->assertRedirect('/login');
        $response->assertSessionHasErrors('email');
        $this->assertTrue(session()->hasOldInput('email'));
        $this->assertFalse(session()->hasOldInput('password'));
        $this->assertGuest();
    }
}
