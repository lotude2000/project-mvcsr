<?php

namespace Tests\Feature\Auth;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    /** @test  */
    public function user_can_view_register_form_if_register_is_exits()
    {
        $response = $this->get('/register');
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('auth.register');
        $response->assertSee('register');
    }

    /** @test  */
    public function user_can_register_if_register_is_exits()
    {
        $this->assertGuest();
        $user = User::factory()->make();
        $response = $this->post('/register', [
            'name' => $user->name,
            'email' => $user->email,
            'password' => '12345678',
            'password_confirmation' => '12345678'
        ]);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/home');
        $this->assertAuthenticated();
    }

    /** @test  */
    public function user_can_not_register_if_register_name_is_required()
    {
        $this->assertGuest();
        $user = User::factory()->make();
        $response = $this->post('/register', [
            'name' => '',
            'email' => $user->email,
            'password' => '12345678',
            'password_confirmation' => '12345678'
        ]);
        $response->assertStatus(Response::HTTP_FOUND);
//        $response->assertRedirect('/home');
        $response->assertSessionHasErrors('name');
    }

    /** @test  */
    public function user_can_not_register_if_register_email_is_required()
    {
        $this->assertGuest();
        $user = User::factory()->make();
        $response = $this->post('/register', [
            'name' => $user->name,
            'email' => '',
            'password' => '12345678',
            'password_confirmation' => '12345678'
        ]);
        $response->assertStatus(Response::HTTP_FOUND);
//        $response->assertRedirect('/home');
        $response->assertSessionHasErrors('email');
    }

    /** @test  */
    public function user_can_not_register_if_register_password_is_required()
    {
        $this->assertGuest();
        $user = User::factory()->make();
        $response = $this->post('/register', [
            'name' => $user->name,
            'email' => $user->email,
            'password' => '',
            'password_confirmation' => '12345678'
        ]);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors('password');
    }

    /** @test  */
    public function user_can_not_register_if_register_password_confirmation_is_required()
    {
        $this->assertGuest();
        $user = User::factory()->make();
        $response = $this->post('/register', [
            'name' => $user->name,
            'email' => $user->email,
            'password' => '12345678',
            'password_confirmation' => ''
        ]);
        $response->assertStatus(Response::HTTP_FOUND);
    }

    /** @test  */
    public function user_can_not_register_if_register_password_is_not_like_password_confirmation_is_required()
    {
        $this->assertGuest();
        $user = User::factory()->make();
        $response = $this->post('/register', [
            'name' => $user->name,
            'email' => $user->email,
            'password' => '12345678',
            'password_confirmation' => '1234567'
        ]);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors('password');
    }
}
