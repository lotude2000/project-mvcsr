<?php

namespace Tests\Feature\Roles;

use App\Models\Role;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetListRoleTest extends TestCase
{

    public function getListRoleRoute($searchField)
    {
        return route('roles.index',$searchField);
    }

    public function createFactoryRole()
    {
        return Role::factory()->create();
    }

    /** @test */
    public function authenticated_super_admin_can_see_all_role_if_role_exits()
    {
        $this->loginWithSuperAdmin();
        $role = $this->createFactoryRole();
        $response = $this->get($this->getListRoleRoute(''));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('roles.index');
        $response->assertSee($role->name);
    }

    /** @test */
    public function authenticated_authorize_user_can_see_all_role_if_role_exits()
    {
        $this->loginUserWithPermission('role_list');
        $role = $this->createFactoryRole();
        $response = $this->get($this->getListRoleRoute(''));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('roles.index');
        $response->assertSee($role->name);
    }

    /** @test */
    public function authenticated_not_authorize_user_can_see_all_role_if_role_exits()
    {
        $this->loginWithUser();
        $response = $this->get($this->getListRoleRoute(''));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function unauthenticated_user_can_see_all_role_if_role_exits()
    {
        $response = $this->get($this->getListRoleRoute(''));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('login');
    }

    /** @test */
    public function authenticated_super_admin_can_search_role_with_role_name_if_role_exits()
    {
        $this->loginWithSuperAdmin();
        $roles = Role::factory()->create();
        $response = $this->get($this->getListRoleRoute($roles->name));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('roles.index');
        $response->assertSee($roles->name);
    }
}
