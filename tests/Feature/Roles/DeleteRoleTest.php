<?php

namespace Tests\Feature\Roles;

use App\Models\Category;
use App\Models\Permission;
use App\Models\Role;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteRoleTest extends TestCase
{

    public function getDeleteRoleRoute($id)
    {
        return route('roles.destroy', $id);
    }

    public function createFactoryRole()
    {
        return Role::factory()->create();
    }

    /** @test  */
    public function authenticated_super_admin_can_delete_role_if_role_is_exits()
    {
        $this->loginWithSuperAdmin();
        $role = $this->createFactoryRole();
        $role->permissions()->attach(Permission::all()->pluck('id'));
        $response = $this->delete($this->getDeleteRoleRoute($role->id));
        $role->permissions()->detach(Permission::all()->pluck('id'));
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseMissing('roles',$role->toArray());
    }

    /** @test  */
    public function authenticated_authorize_user_can_delete_role_if_role_is_exits()
    {
        $this->loginUserWithPermission('role_delete');
        $role = $this->createFactoryRole();
        $role->permissions()->attach(Permission::all()->pluck('id'));
        $response = $this->delete($this->getDeleteRoleRoute($role->id));
        $role->permissions()->detach(Permission::all()->pluck('id'));
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseMissing('roles',$role->toArray());
    }

    /** @test  */
    public function authenticated_not_authorize_user_can_not_delete_role_if_role_is_exits()
    {
        $this->loginWithUser();
        $role = $this->createFactoryRole();
        $response = $this->delete($this->getDeleteRoleRoute($role->id));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test  */
    public function unauthenticated_not_authorize_user_can_not_delete_role_if_role_is_exits()
    {
        $role = $this->createFactoryRole();
        $response = $this->delete($this->getDeleteRoleRoute($role->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('login');
    }

    /** @test  */
    public function authenticated_authorize_user_can_delete_role_if_role_id_not_found()
    {
        $this->loginUserWithPermission('role_delete');
        $roleId = -1;
        $response = $this->delete($this->getDeleteRoleRoute($roleId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
