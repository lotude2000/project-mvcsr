<?php

namespace Tests\Feature\Roles;

use App\Models\Role;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class ShowRoleTest extends TestCase
{

    public function getShowRoleTest($id)
    {
        return route('roles.show',$id);
    }

    public function createFactoryRole()
    {
        return Role::factory()->create();
    }

    /** @test  */
    public function authenticated_super_admin_can_get_role_if_role_exits()
    {
        $this->loginWithSuperAdmin();
        $role =$this->createFactoryRole();
        $response = $this->get($this->getShowRoleTest($role->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('roles.show');
        $response->assertSee($role->name);
    }

    /** @test  */
    public function authenticated_authorize_user_can_get_role_if_role_exits()
    {
        $this->loginUserWithPermission('role_show');
        $role =$this->createFactoryRole();
        $response = $this->get($this->getShowRoleTest($role->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('roles.show');
        $response->assertSee($role->name);
    }

    /** @test  */
    public function authenticated_not_authorize_user_can_not_get_role_if_role_exits()
    {
        $this->loginWithUser();
        $role =$this->createFactoryRole();
        $response = $this->get($this->getShowRoleTest($role->id));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test  */
    public function unauthenticated_user_can_not_get_role_if_role_exits()
    {
        $role =$this->createFactoryRole();
        $response = $this->get($this->getShowRoleTest($role->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('login');
    }

    /** @test  */
    public function authenticated_authorize_user_can_not_get_role_if_role_id_is_not_exits()
    {
        $this->loginUserWithPermission('role_show');
        $roleId = -1;
        $response = $this->get($this->getShowRoleTest($roleId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
