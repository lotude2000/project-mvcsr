<?php

namespace Tests\Feature\Roles;

use App\Models\Role;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateNewRoleTest extends TestCase
{

    public function getStoreRoleRoute()
    {
        return route('roles.store');
    }

    public function getCreateRoleRoute()
    {
        return route('roles.create');
    }

    public function makeFactoryRole()
    {
        return Role::factory()->make()->toArray();
    }

    /** @test */
    public function authenticated_super_admin_can_view_create_role_form_if_role_is_exits()
    {
        $this->loginWithSuperAdmin();
        $response = $this->get($this->getCreateRoleRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('roles.create');
    }

    /** @test */
    public function authenticated_authorize_user_can_view_create_role_form_if_role_is_exits()
    {
        $this->loginUserWithPermission('role_create');
        $response = $this->get($this->getCreateRoleRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('roles.create');
    }

    /** @test */
    public function authenticated_not_authorize_user_can_not_view_create_role_form_if_role_is_exits()
    {
        $this->loginWithUser();
        $response = $this->get($this->getCreateRoleRoute());
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function unauthenticated_user_can_not_view_create_role_form_if_role_is_exits()
    {
        $response = $this->get($this->getCreateRoleRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('login');
    }

    /** @test */
    public function authenticated_super_admin_can_create_new_role_if_role_is_exits()
    {
        $this->loginWithSuperAdmin();
        $dataCreate = $this->makeFactoryRole();
        $response = $this->post($this->getStoreRoleRoute(),$dataCreate);
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('roles',$dataCreate);
        $response->assertRedirect(route('roles.index'));
    }

    /** @test */
    public function authenticated_authorize_user_can_create_new_role_if_role_is_exits()
    {
        $this->loginUserWithPermission('role_store');
        $dataCreate = $this->makeFactoryRole();
        $response = $this->post($this->getStoreRoleRoute(),$dataCreate);
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('roles',$dataCreate);
        $response->assertRedirect(route('roles.index'));
    }

    /** @test */
    public function authenticated_authorize_user_can_not_create_new_role_if_role_is_exits()
    {
        $this->loginWithUser();
        $dataCreate = $this->makeFactoryRole();
        $response = $this->post($this->getStoreRoleRoute(),$dataCreate);
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function authenticated_authorize_user_can_not_create_new_role_if_role_name_is_not_validate()
    {
        $this->loginWithSuperAdmin();
        $dataCreate = Role::factory()->make([
            'name'=>''
        ])->toArray();
        $response = $this->post($this->getStoreRoleRoute(),$dataCreate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function authenticated_authorize_user_can_not_create_new_role_if_role_description_is_not_validate()
    {
        $this->loginWithSuperAdmin();
        $dataCreate = Role::factory()->make([
            'description'=>''
        ])->toArray();
        $response = $this->post($this->getStoreRoleRoute(),$dataCreate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['description']);
    }

    /** @test */
    public function authenticated_authorize_user_can_not_create_new_role_if_role_data_is_not_validate()
    {
        $this->loginWithSuperAdmin();
        $dataCreate = Role::factory()->make([
            'name'=>'',
            'description'=>''
        ])->toArray();
        $response = $this->post($this->getStoreRoleRoute(),$dataCreate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name','description']);
    }


}
