<?php

namespace Tests\Feature\Roles;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class UpdateRoleTest extends TestCase
{

    public function getEditRoleRoute($id)
    {
        return route('roles.edit',$id);
    }

    public function getUpdateUserRoute($id)
    {
        return route('roles.update',$id);
    }

    public function createFactoryRole()
    {
        return Role::factory()->create();
    }

    public function makeFactoryRole()
    {
        return Role::factory()->make()->toArray();
    }

    /** @test  */
    public function authenticated_super_admin_can_view_edit_role_form_if_role_is_exits()
    {
        $this->loginWithSuperAdmin();
        $role = $this->createFactoryRole();
        $response = $this->get($this->getEditRoleRoute($role->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('roles.edit');
    }

    /** @test  */
    public function authenticated_authorize_user_can_view_edit_role_form_if_role_is_exits()
    {
        $this->loginUserWithPermission('role_edit');
        $role = $this->createFactoryRole();
        $response = $this->get($this->getEditRoleRoute($role->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('roles.edit');
    }

    /** @test  */
    public function authenticated_not_authorize_user_can_not_view_edit_role_form_if_role_is_exits()
    {
        $this->loginWithUser();
        $role = $this->createFactoryRole();
        $response = $this->get($this->getEditRoleRoute($role->id));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test  */
    public function authenticated_authorize_user_can_not_view_edit_role_form_if_role_id_not_found()
    {
        $this->loginUserWithPermission('role_edit');
        $roleId = -1;
        $response = $this->get($this->getEditRoleRoute($roleId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test  */
    public function authenticated_super_admin_can_update_role_if_role_is_exits()
    {
        $this->loginWithSuperAdmin();
        $role = $this->createFactoryRole();
        $role->permissions()->attach(Permission::all()->pluck('id'));
        $dataUpdate = $this->makeFactoryRole();
        $role->permissions()->sync(Permission::all()->pluck('id'));
        $response = $this->patch($this->getUpdateUserRoute($role->id), $dataUpdate);
        $this->assertDatabaseHas('roles',$dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('roles.index'));
    }

    /** @test  */
    public function authenticated_authorize_user_can_update_role_if_role_is_exits()
    {
        $this->loginUserWithPermission('role_update');
        $role = $this->createFactoryRole();
        $role->permissions()->attach(Permission::all()->pluck('id'));
        $dataUpdate = $this->makeFactoryRole();
        $role->permissions()->sync(Permission::all()->pluck('id'));
        $response = $this->patch($this->getUpdateUserRoute($role->id), $dataUpdate);
        $this->assertDatabaseHas('roles',$dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('roles.index'));
    }

    /** @test  */
    public function authenticated_not_authorize_user_can_not_update_role_if_role_is_exits()
    {
        $this->loginWithUser();
        $role = $this->createFactoryRole();
        $response = $this->patch($this->getUpdateUserRoute($role->id));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test  */
    public function authenticated_authorize_user_can_not_update_role_if_role_name_is_not_validate()
    {
        $this->loginUserWithPermission('role_update');
        $role = $this->createFactoryRole();
        $dataUpdate = Role::factory()->make([
            'name'=>''
        ])->toArray();
        $response = $this->patch($this->getUpdateUserRoute($role->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }

    /** @test  */
    public function authenticated_authorize_user_can_not_update_role_if_role_description_is_not_validate()
    {
        $this->loginUserWithPermission('role_update');
        $role = $this->createFactoryRole();
        $dataUpdate = Role::factory()->make([
            'description'=>''
        ])->toArray();
        $response = $this->patch($this->getUpdateUserRoute($role->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['description']);
    }

    /** @test  */
    public function authenticated_authorize_user_can_not_update_role_if_role_data_is_not_validate()
    {
        $this->loginUserWithPermission('role_update');
        $role = $this->createFactoryRole();
        $dataUpdate = Role::factory()->make([
            'name'=>'',
            'description'=>''
        ])->toArray();
        $response = $this->patch($this->getUpdateUserRoute($role->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name','description']);
    }
}
