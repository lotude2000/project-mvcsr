<?php

namespace Tests\Feature\Categories;

use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class UpdateCategoryTest extends TestCase
{

    public function getUpdateCategoryRoute($id)
    {
        return route('categories.update',$id);
    }

    /** @test  */
    public function authenticated_super_admin_can_update_category_if_category_exits()
    {
        $this->loginWithSuperAdmin();
        $category = Category::factory()->create()->toArray();
        $dataUpdate = Category::factory()->make()->toArray();
        $response = $this->put($this->getUpdateCategoryRoute($category['id']),$dataUpdate);
        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseHas('categories', $dataUpdate);
        $response->assertJson( fn(AssertableJson $json)=>
        $json->where('status',Response::HTTP_OK)
            ->where('message','success')
            ->etc()
        );
    }

    /** @test  */
    public function authenticated_authorize_user_can_update_category_if_category_exits()
    {
        $this->loginUserWithPermission('category_update');
        $category = Category::factory()->create()->toArray();
        $dataUpdate = Category::factory()->make()->toArray();
        $response = $this->put($this->getUpdateCategoryRoute($category['id']),$dataUpdate);
        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseHas('categories', $dataUpdate);
        $response->assertJson( fn(AssertableJson $json)=>
        $json->where('status',Response::HTTP_OK)
            ->where('message','success')
            ->etc()
        );
    }

    /** @test  */
    public function authenticated_not_authorize_user_can_not_update_category_if_category_exits()
    {
        $this->loginWithUser();
        $category = Category::factory()->create()->toArray();
        $response = $this->put($this->getUpdateCategoryRoute($category['id']));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test  */
    public function unauthenticated_user_can_not_update_category_if_category_exits()
    {
        $category = Category::factory()->create()->toArray();
        $response = $this->put($this->getUpdateCategoryRoute($category['id']));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('login');
    }

    /** @test  */
    public function authenticated_super_admin_can_not_update_category_if_category_id_not_exits()
    {
        $this->loginWithSuperAdmin();
        $categoryId = -1;
        $dataUpdate = Category::factory()->make()->toArray();
        $response = $this->put($this->getUpdateCategoryRoute($categoryId),$dataUpdate);
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test  */
    public function authenticated_super_admin_can_not_update_category_if_category_name_not_validate()
    {
        $this->loginWithSuperAdmin();
        $category = Category::factory()->create()->toArray();
        $dataUpdate = Category::factory()->make([
            'name'=>''
        ])->toArray();
        $response = $this->put($this->getUpdateCategoryRoute($category['id']),$dataUpdate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn(AssertableJson $json) =>
            $json->has('errors', fn(AssertableJson $json) =>
                $json->has('name', fn(AssertableJson $json) =>
                    $json->where('0','The name field is required.')
                )
            )
        );
    }

    /** @test  */
    public function authenticated_super_admin_can_not_update_category_if_category_parent_id_not_validate()
    {
        $this->loginWithSuperAdmin();
        $category = Category::factory()->create()->toArray();
        $dataUpdate = Category::factory()->make([
            'parent_id'=>''
        ])->toArray();
        $response = $this->put($this->getUpdateCategoryRoute($category['id']),$dataUpdate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn(AssertableJson $json) =>
            $json->has('errors', fn(AssertableJson $json) =>
                $json->has('parent_id', fn(AssertableJson $json) =>
                    $json->where('0','The parent id field is required.')
                )
            )
        );
    }

    /** @test  */
    public function authenticated_super_admin_can_not_update_category_if_category_data_validate()
    {
        $this->loginWithSuperAdmin();
        $category = Category::factory()->create()->toArray();
        $dataUpdate = Category::factory()->make([
            'name'=>'',
            'parent_id' => ''
        ])->toArray();
        $response = $this->put($this->getUpdateCategoryRoute($category['id']),$dataUpdate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn(AssertableJson $json) =>
            $json->has('errors', fn(AssertableJson $json) =>
                $json->has('name')
                    ->has('parent_id')
                    ->etc()
            )
        );
    }
}
