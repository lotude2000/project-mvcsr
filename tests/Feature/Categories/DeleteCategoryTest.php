<?php

namespace Tests\Feature\Categories;

use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteCategoryTest extends TestCase
{

    public function getDeleteCategoryRoute($id)
    {
        return route('categories.destroy',$id);
    }

    /** @test */
    public function authenticated_super_admin_can_delete_category_if_category_is_exist()
    {
        $this->loginWithSuperAdmin();
        $Category = Category::factory()->create()->toArray();
        $response = $this->delete($this->getDeleteCategoryRoute($Category['id']));
        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseMissing('categories',$Category);
    }

    /** @test */
    public function authenticated_authorize_user_can_delete_category_if_category_is_exist()
    {
        $this->loginUserWithPermission('category_delete');
        $Category = Category::factory()->create()->toArray();
        $response = $this->delete($this->getDeleteCategoryRoute($Category['id']));
        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseMissing('categories',$Category);
    }

    /** @test */
    public function authenticated_not_authorize_user_can_not_delete_category_if_category_is_exist()
    {
        $this->loginWithUser();
        $Category = Category::factory()->create()->toArray();
        $response = $this->delete($this->getDeleteCategoryRoute($Category['id']));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function unauthenticated_user_can_not_delete_category_if_category_is_exist()
    {
        $Category = Category::factory()->create()->toArray();
        $response = $this->delete($this->getDeleteCategoryRoute($Category['id']));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('login');
    }

    /** @test */
    public function authenticated_super_admin_can_not_delete_category_if_category_id_not_exist()
    {
        $this->loginWithSuperAdmin();
        $categoryId = -1;
        $response = $this->delete($this->getDeleteCategoryRoute($categoryId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
