<?php

namespace Tests\Feature\Categories;

use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class ShowCategoryTest extends TestCase
{

    public function getEditCategoryRoute($id)
    {
        return route('categories.edit',$id);
    }

    /** @test  */
    public function authenticated_super_admin_can_view_update_category_form_if_category_exits()
    {
        $this->loginWithSuperAdmin();
        $category = Category::factory()->create()->toArray();
        $response = $this->get($this->getEditCategoryRoute($category['id']));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('categories.edit');
        $response->assertSee($category['name']);
    }

    /** @test  */
    public function authenticated_authorize_user_can_view_update_category_form_if_category_exits()
    {
        $this->loginUserWithPermission('category_edit');
        $category = Category::factory()->create()->toArray();
        $response = $this->get($this->getEditCategoryRoute($category['id']));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('categories.edit');
        $response->assertSee($category['name']);
    }

    /** @test  */
    public function authenticated_not_authorize_user_can_not_view_update_category_form_if_category_exits()
    {
        $this->loginWithUser();
        $category = Category::factory()->create()->toArray();
        $response = $this->get($this->getEditCategoryRoute($category['id']));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test  */
    public function unauthenticated_user_can_not_view_update_category_form_if_category_exits()
    {
        $category = Category::factory()->create()->toArray();
        $response = $this->get($this->getEditCategoryRoute($category['id']));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('login');
    }

    /** @test  */
    public function authenticated_super_admin_can_not_view_update_category_form_if_category_id_not_exits()
    {
        $this->loginWithSuperAdmin();
        $categoryId = -1;
        $dataEdit = Category::factory()->make()->toArray();
        $response = $this->get($this->getEditCategoryRoute($categoryId), $dataEdit);
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
