<?php

namespace Tests\Feature\Categories;

use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetListCategoryTest extends TestCase
{

    public function getListCategoryRoute($searchField)
    {
        return route('categories.index',$searchField);
    }

    /** @test  */
    public function unauthenticated_user_can_not_get_list_category_if_category_exits()
    {
        $response = $this->get($this->getListCategoryRoute(''));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('login');
    }

    /** @test  */
    public function authenticated_not_authorize_user_can_not_get_list_category_if_category_exits()
    {
        $this->loginWithUser();
        $response = $this->get($this->getListCategoryRoute(''));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test  */
    public function authenticated_authorize_user_can_get_list_category_if_category_exits()
    {
        $this->loginUserWithPermission('category_list');
        $response = $this->get($this->getListCategoryRoute(''));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('categories.index');
    }

    /** @test  */
    public function authenticated_super_admin_can_get_list_category_if_category_exits()
    {
        $this->loginWithSuperAdmin();
        $response = $this->get($this->getListCategoryRoute(''));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('categories.index');
    }

    /** @test  */
    public function authenticated_super_admin_can_search_category_with_category_name_if_category_exits()
    {
        $this->loginWithSuperAdmin();
        $category = Category::factory()->create();
        $response = $this->get($this->getListCategoryRoute($category->name));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('categories.index');
        $response->assertSee($category->name);
    }

    /** @test  */
    public function authenticated_super_admin_can_search_category_with_category_parent_id_if_category_exits()
    {
        $this->loginWithSuperAdmin();
        $category = Category::factory()->create();
        $response = $this->get($this->getListCategoryRoute($category->parent_id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('categories.index');
        $response->assertSee($category->name);
    }

    /** @test  */
    public function authenticated_super_admin_can_search_category_with_category_parent_id_and_name_if_category_exits()
    {
        $this->loginWithSuperAdmin();
        $category = Category::factory()->create(
            [
                'parent_id'=>$this->faker->randomElement(Category::all()->pluck('id'))
            ]
        );
        $response = $this->get($this->getListCategoryRoute(
            [
                $category->name,
                $category->parent_id
            ]
        ));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('categories.index');
        $response->assertSee($category->name);
    }
}
