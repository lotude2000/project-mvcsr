<?php

namespace Tests\Feature\Categories;

use App\Models\Category;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class CreateNewCategoryTest extends TestCase
{

    public function getStoreCategoryRoute()
    {
        return route('categories.store');
    }

    public function makeCategoryRoute()
    {
        return Category::factory()->make()->toArray();
    }

    /** @test  */
    public function authenticated_super_admin_can_create_new_category_if_category_exits()
    {
        $this->loginWithSuperAdmin();
        $dataCreate = $this->makeCategoryRoute();
        $response = $this->post($this->getStoreCategoryRoute(),$dataCreate);
        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseHas('categories', $dataCreate);
        $response->assertJson( fn(AssertableJson $json)=>
            $json->where('status',Response::HTTP_OK)
            ->where('message','success')
            ->etc()
        );
    }

    /** @test  */
    public function authenticated_authorize_user_can_create_new_category_if_category_exits()
    {
        $this->loginUserWithPermission('category_store');
        $dataCreate = $this->makeCategoryRoute();
        $response = $this->post($this->getStoreCategoryRoute(),$dataCreate);
        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseHas('categories', $dataCreate);
        $response->assertJson( fn(AssertableJson $json)=>
        $json->where('status',Response::HTTP_OK)
            ->where('message','success')
            ->etc()
        );
    }

    /** @test  */
    public function authenticated_not_authorize_user_can_not_create_new_category_if_category_exits()
    {
        $this->loginWithUser();
        $dataCreate = $this->makeCategoryRoute();
        $response = $this->postJson($this->getStoreCategoryRoute(),$dataCreate);
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test  */
    public function unauthenticated_user_can_not_create_new_category_if_category_exits()
    {
        $dataCreate = $this->makeCategoryRoute();
        $response = $this->post($this->getStoreCategoryRoute(),$dataCreate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('login');
    }

    /** @test  */
    public function authenticated_super_admin_can_not_create_new_category_if_category_name_not_validate()
    {
        $this->loginWithSuperAdmin();
        $dataCreate = Category::factory()->make(['name' => ''])->toArray();
        $response = $this->postJson($this->getStoreCategoryRoute(),$dataCreate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn(AssertableJson $json) =>
            $json->has('errors', fn(AssertableJson $json) =>
                $json->has('name', fn(AssertableJson $json) =>
                    $json->where('0','The name field is required.')
                 )
            )
        );
    }

    /** @test  */
    public function authenticated_super_admin_can_not_create_new_category_if_category_parent_id_not_validate()
    {
        $this->loginWithSuperAdmin();
        $dataCreate = Category::factory()->make(['parent_id' => ''])->toArray();
        $response = $this->postJson($this->getStoreCategoryRoute(),$dataCreate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn(AssertableJson $json) =>
            $json->has('errors', fn(AssertableJson $json) =>
                $json->has('parent_id', fn(AssertableJson $json) =>
                    $json->where('0','The parent id field is required.')
                )
            )
        );
    }

    /** @test  */
    public function authenticated_super_admin_can_not_create_new_category_if_category_data_not_validate()
    {
        $this->loginWithSuperAdmin();
        $dataCreate = Category::factory()->make([
            'name'=>'',
            'parent_id' => ''
        ])->toArray();
        $response = $this->postJson($this->getStoreCategoryRoute(),$dataCreate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn(AssertableJson $json) =>
            $json->has('errors', fn(AssertableJson $json) =>
                $json->has('name')
                ->has('parent_id')
                ->etc()
                )
        );
    }
}
