<?php

namespace Tests\Feature\Users;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateNewUserTest extends TestCase
{

    public function getCreateUserRoute()
    {
        return route('users.create');
    }

    public function getStoreUserRoute()
    {
        return route('users.store');
    }

    /** @test  */
    public function authenticated_super_admin_can_view_create_user_form_if_user_is_exits()
    {
        $this->loginWithSuperAdmin();
        $response = $this->get($this->getCreateUserRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('users.create');
    }

    /** @test */
    public function authenticated_authorize_user_can_view_create_user_form_if_user_is_exits()
    {
        $this->loginUserWithPermission('user_create');
        $response = $this->get($this->getCreateUserRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('users.create');
    }

    /** @test */
    public function authenticated_not_authorize_user_can_not_view_create_user_form_if_user_is_exits()
    {
        $this->loginWithUser();
        $response = $this->get($this->getCreateUserRoute());
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function unauthenticated_user_can_not_view_create_product_form_if_product_is_exits()
    {
        $response = $this->get($this->getCreateUserRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('login');
    }

    /** @test */
    public function authenticated_super_admin_can_create_new_user_if_user_is_exits()
    {
        $this->loginWithSuperAdmin();
        $dataCreate = User::factory()->make()->toArray();
        $dataCreate['password'] = '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi';
        $response = $this->post($this->getStoreUserRoute(), $dataCreate);
        array_splice($dataCreate, 2);
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('users',$dataCreate);
        $response->assertRedirect(route('users.index'));
    }

    /** @test */
    public function authenticated_super_admin_can_not_create_new_user_if_name_field_is_not_validate()
    {
        $this->loginWithSuperAdmin();
        $dataCreate = User::factory()->make([
            'name'=>''
        ])->toArray();
        $response = $this->post($this->getStoreUserRoute(), $dataCreate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }


    /** @test */
    public function authenticated_super_admin_can_not_create_new_user_if_email_field_is_not_validate()
    {
        $this->loginWithSuperAdmin();
        $dataCreate = User::factory()->make([
            'email'=>''
        ])->toArray();
        $response = $this->post($this->getStoreUserRoute(), $dataCreate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['email']);
    }

    /** @test */
    public function authenticated_super_admin_can_not_create_new_user_if_password_field_is_not_validate()
    {
        $this->loginWithSuperAdmin();
        $dataCreate = User::factory()->make([
            'password'=>''
        ])->toArray();
        $response = $this->post($this->getStoreUserRoute(), $dataCreate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['password']);
    }

    /** @test */
    public function authenticated_super_admin_can_not_create_new_user_if_data_is_not_validate()
    {
        $this->loginWithSuperAdmin();
        $dataCreate = User::factory()->make([
            'name'=>'',
            'email'=>'',
            'password'=>''
        ])->toArray();
        $response = $this->post($this->getStoreUserRoute(), $dataCreate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name','email','password']);
    }

    /** @test */
    public function unauthenticated_user_can_not_create_new_user_if_user_exits()
    {
        $dataCreate = User::factory()->make()->toArray();
        $response = $this->post($this->getStoreUserRoute(), $dataCreate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('login'));
    }
}
