<?php

namespace Tests\Feature\Users;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class ShowUserTest extends TestCase
{

    public function getShowUserTest($id)
    {
        return route('users.show',$id);
    }

    public function createFactoryUser()
    {
        return User::factory()->create();
    }

    public function createFactoryRole()
    {
        return Role::factory()->create();
    }

    /** @test  */
    public function authenticated_super_admin_can_get_user_if_user_exits()
    {
        $this->loginWithSuperAdmin();
        $user = $this->createFactoryUser();
        $role =$this->createFactoryRole();
        $user->roles()->attach($role->pluck('id'));
        $response = $this->get($this->getShowUserTest($user->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('users.show');
        $response->assertSee($user->name);
    }

    /** @test  */
    public function authenticated_authorize_user_can_get_user_if_user_exits()
    {
        $this->loginUserWithPermission('user_show');
        $user = $this->createFactoryUser();
        $role =$this->createFactoryRole();
        $user->roles()->attach($role->pluck('id'));
        $response = $this->get($this->getShowUserTest($user->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('users.show');
        $response->assertSee($user->name);
    }

    /** @test  */
    public function authenticated_not_authorize_user_can_not_get_user_if_user_exits()
    {
        $this->loginWithUser();
        $user = $this->createFactoryUser();
        $role =$this->createFactoryRole();
        $user->roles()->attach($role->pluck('id'));
        $response = $this->get($this->getShowUserTest($user->id));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test  */
    public function unauthenticated_user_can_not_get_user_if_user_exits()
    {
        $user = $this->createFactoryUser();
        $response = $this->get($this->getShowUserTest($user->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('login');
    }

    /** @test  */
    public function authenticated_authorize_user_can_not_get_user_if_user_id_is_not_exits()
    {
        $this->loginUserWithPermission('user_show');
        $userId = -1;
        $response = $this->get($this->getShowUserTest($userId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
