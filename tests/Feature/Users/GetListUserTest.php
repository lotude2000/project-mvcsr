<?php

namespace Tests\Feature\Users;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetListUserTest extends TestCase
{

    public function getListUserRoute($searchField)
    {
        return route('users.index', $searchField);
    }

    public function createFactoryUser()
    {
        return User::factory()->create();
    }

    /** @test */
    public function authenticated_super_admin_can_see_all_user_if_user_exits()
    {
        $this->loginWithSuperAdmin();
        $product = $this->createFactoryUser();
        $response = $this->get($this->getListUserRoute(''));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('users.index');
        $response->assertSee($product->name);
    }

    /** @test */
    public function authenticated_authorize_user_can_see_all_user_if_user_exits()
    {
        $this->loginUserWithPermission('user_list');
        $product = $this->createFactoryUser();
        $response = $this->get($this->getListUserRoute(''));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('users.index');
        $response->assertSee($product->name);
    }

    /** @test */
    public function unauthenticated_user_can_not_see_all_user_if_user_exits()
    {
        $response = $this->get($this->getListUserRoute(''));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function authenticated_not_authorize_user_can_not_see_all_user_if_user_exits()
    {
        $this->loginWithUser();
        $response = $this->get($this->getListUserRoute(''));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function authenticated_super_admin_can_search_user_with_user_name_if_name_exits()
    {
        $this->loginWithSuperAdmin();
        $user = User::factory()->create();
        $response = $this->get($this->getListUserRoute($user->name));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('users.index');
        $response->assertSee($user->name);
    }

    /** @test */
    public function authenticated_super_admin_can_search_user_with_user_email_if_email_exits()
    {
        $this->loginWithSuperAdmin();
        $user = User::factory()->create();
        $response = $this->get($this->getListUserRoute($user->email));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('users.index');
        $response->assertSee($user->email);
    }

    /** @test */
    public function authenticated_super_admin_can_search_user_with_role_id_if_email_exits()
    {
        $this->loginWithSuperAdmin();
        $user = User::factory()->create();
        $role = Role::factory()->create();
        $user->roles()->attach($role->id);
        $response = $this->get($this->getListUserRoute($role->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('users.index');
        $response->assertSee($user->name);
    }

    /** @test */
    public function authenticated_super_admin_can_search_user_with_user_name_email_and_role_id_if_email_exits()
    {
        $this->loginWithSuperAdmin();
        $user = User::factory()->create();
        $role = Role::factory()->create();
        $user->roles()->attach($role->id);
        $response = $this->get($this->getListUserRoute(
            [
                $user->name,
                $user->email,
                $role->id
            ]
        ));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('users.index');
        $response->assertSee($user->name, $user->email);
    }
}
