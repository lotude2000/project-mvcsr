<?php

namespace Tests\Feature\Users;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class UpdateUserTest extends TestCase
{

    public function getEditUserRoute($id)
    {
        return route('users.edit',$id);
    }

    public function getUpdateUserRoute($id)
    {
        return route('users.update',$id);
    }

    public function createFactoryUser()
    {
        return User::factory()->create();
    }

    public function makeFactoryUser()
    {
        return User::factory()->make()->toArray();
    }

    /** @test  */
    public function authenticated_super_admin_can_view_edit_user_form_if_user_is_exits()
    {
        $this->loginWithSuperAdmin();
        $user = $this->createFactoryUser();
        $response = $this->get($this->getEditUserRoute($user->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('users.edit');
    }

    /** @test  */
    public function authenticated_authorize_user_can_view_edit_user_form_if_user_is_exits()
    {
        $this->loginUserWithPermission('user_edit');
        $user = $this->createFactoryUser();
        $response = $this->get($this->getEditUserRoute($user->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('users.edit');
    }

    /** @test  */
    public function authenticated_not_authorize_user_can_not_view_edit_user_form_if_user_is_exits()
    {
        $this->loginWithUser();
        $user = $this->createFactoryUser();
        $response = $this->get($this->getEditUserRoute($user->id));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test  */
    public function authenticated_super_admin_can_not_view_edit_user_form_if_user_id_is_not_exits()
    {
        $this->loginWithSuperAdmin();
        $userId = -1;
        $response = $this->get($this->getEditUserRoute($userId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test  */
    public function unauthenticated_user_can_not_view_edit_user_form_if_user_is_exits()
    {
        $product = $this->createFactoryUser();
        $response = $this->get($this->getEditUserRoute($product->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('login');
    }

    /** @test  */
    public function authenticated_authorize_user_can_update_user_if_user_is_exits()
    {
        $this->loginUserWithPermission('user_update');
        $user = $this->createFactoryUser();
        $role = Role::factory()->create();
        $user->roles()->attach($role->pluck('id'));
        $dataUpdate = $this->makeFactoryUser();
        $dataUpdate['password'] = '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi';
        $roleUpdate = Role::factory()->create();
        $user->roles()->sync($roleUpdate->pluck('id'));
        $response = $this->patch($this->getUpdateUserRoute($user->id), $dataUpdate);
        array_splice($dataUpdate, 2);
        $this->assertDatabaseHas('users',$dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('users.index'));
    }

    /** @test  */
    public function authenticated_not_authorize_user_can_update_user_if_user_is_exits()
    {
        $this->loginWithUser();
        $user = $this->createFactoryUser();
        $dataUpdate = $this->makeFactoryUser();
        $dataUpdate['password'] = '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi';
        $response = $this->patch($this->getUpdateUserRoute($user->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test  */
    public function authenticated_authorize_user_can_not_update_user_if_user_name_is_not_validate()
    {
        $this->loginUserWithPermission('user_update');
        $user = $this->createFactoryUser();
        $role = Role::factory()->create();
        $user->roles()->attach($role->pluck('id'));
        $dataUpdate = User::factory()->make([
            'name'=>''
        ])->toArray();
        $dataUpdate['password'] = '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi';
        $roleUpdate = Role::factory()->create();
        $user->roles()->sync($roleUpdate->pluck('id'));
        $response = $this->patch($this->getUpdateUserRoute($user->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }

    /** @test  */
    public function authenticated_authorize_user_can_not_update_user_if_user_email_is_not_validate()
    {
        $this->loginUserWithPermission('user_update');
        $user = $this->createFactoryUser();
        $role = Role::factory()->create();
        $user->roles()->attach($role->pluck('id'));
        $dataUpdate = User::factory()->make([
            'email'=>''
        ])->toArray();
        $dataUpdate['password'] = '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi';
        $roleUpdate = Role::factory()->create();
        $user->roles()->sync($roleUpdate->pluck('id'));
        $response = $this->patch($this->getUpdateUserRoute($user->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['email']);
    }

    /** @test  */
    public function authenticated_authorize_user_can_not_update_user_if_user_password_is_not_validate()
    {
        $this->loginUserWithPermission('user_update');
        $user = $this->createFactoryUser();
        $role = Role::factory()->create();
        $user->roles()->attach($role->pluck('id'));
        $dataUpdate = $this->makeFactoryUser();
        $roleUpdate = Role::factory()->create();
        $user->roles()->sync($roleUpdate->pluck('id'));
        $response = $this->patch($this->getUpdateUserRoute($user->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['password']);
    }

    /** @test  */
    public function authenticated_authorize_user_can_not_update_user_if_user_data_is_not_validate()
    {
        $this->loginUserWithPermission('user_update');
        $user = $this->createFactoryUser();
        $role = Role::factory()->create();
        $user->roles()->attach($role->pluck('id'));
        $dataUpdate = User::factory()->make([
            'name'=>'',
            'email'=>''
        ])->toArray();
        $roleUpdate = Role::factory()->create();
        $user->roles()->sync($roleUpdate->pluck('id'));
        $response = $this->patch($this->getUpdateUserRoute($user->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name','email','password']);
    }
}
