<?php

namespace Tests\Feature\Users;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteUsersTest extends TestCase
{

    public function getDeleteUserRoute($id)
    {
        return route('users.destroy',$id);
    }

    public function createFactoryUser()
    {
        return User::factory()->create();
    }

    /** @test  */
    public function authenticated_super_admin_can_delete_user_if_user_is_exits()
    {
        $this->loginWithSuperAdmin();
        $user = $this->createFactoryUser();
        $role = Role::factory()->create();
        $user->roles()->attach($role->id);
        $response = $this->delete($this->getDeleteUserRoute($user->id));
        $user->roles()->detach($role->id);
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseMissing('users',$user->toArray());
    }

    /** @test  */
    public function authenticated_authorize_user_can_delete_user_if_user_is_exits()
    {
        $this->loginUserWithPermission('user_delete');
        $user = $this->createFactoryUser();
        $role = Role::factory()->create();
        $user->roles()->attach($role->id);
        $response = $this->delete($this->getDeleteUserRoute($user->id));
        $user->roles()->detach($role->id);
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseMissing('users',$user->toArray());
    }

    /** @test  */
    public function authenticated_not_authorize_user_can_not_delete_user_if_user_is_exits()
    {
        $this->loginWithUser();
        $user = $this->createFactoryUser();
        $response = $this->delete($this->getDeleteUserRoute($user->id));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test  */
    public function authenticated_authorize_user_can_not_delete_user_if_user_id_is_not_exits()
    {
        $this->loginUserWithPermission('user_delete');
        $userId = -1;
        $response = $this->delete($this->getDeleteUserRoute($userId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
