<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('categories')->group(function () {
    Route::get('/',[\App\Http\Controllers\CategoryController::class,'index'])->name('categories.index')->middleware('permission:category_list');
    Route::get('/create',[\App\Http\Controllers\CategoryController::class,'create'])->name('categories.create')->middleware('permission:category_create');
    Route::post('/store',[\App\Http\Controllers\CategoryController::class,'store'])->name('categories.store')->middleware('permission:category_store');
    Route::get('/list',[\App\Http\Controllers\CategoryController::class,'list'])->name('categories.list')->middleware('permission:category_list');
    Route::get('/search',[\App\Http\Controllers\CategoryController::class,'search'])->name('categories.search');
    Route::get('/edit/{id}',[\App\Http\Controllers\CategoryController::class,'edit'])->name('categories.edit')->middleware('permission:category_edit');
    Route::put('/update/{id}',[\App\Http\Controllers\CategoryController::class,'update'])->name('categories.update')->middleware('permission:category_update');
    Route::delete('/delete/{id}',[\App\Http\Controllers\CategoryController::class,'delete'])->name('categories.destroy')->middleware('permission:category_delete');
});
