<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('products')->group(function () {
    Route::get('/',[\App\Http\Controllers\ProductController::class,'index'])->name('products.index')->middleware('permission:product_list');
    Route::get('/create',[\App\Http\Controllers\ProductController::class,'create'])->name('products.create')->middleware('permission:product_create');
    Route::post('/store',[\App\Http\Controllers\ProductController::class,'store'])->name('products.store')->middleware('permission:product_store');
    Route::get('/{id}',[\App\Http\Controllers\ProductController::class,'show'])->name('products.show')->middleware('permission:product_show');
    Route::get('//edit/{id}',[\App\Http\Controllers\ProductController::class,'edit'])->name('products.edit')->middleware('permission:product_edit');
    Route::patch('//update/{id}',[\App\Http\Controllers\ProductController::class,'update'])->name('products.update')->middleware('permission:product_update');
    Route::delete('/delete/{id}',[\App\Http\Controllers\ProductController::class,'delete'])->name('products.destroy')->middleware('permission:product_delete');
});
