<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('users')->group(function () {
    Route::get('/',[\App\Http\Controllers\UserController::class,'index'])->name('users.index')->middleware('permission:user_list');
    Route::get('/create',[\App\Http\Controllers\UserController::class,'create'])->name('users.create')->middleware('permission:user_create');
    Route::post('/store',[\App\Http\Controllers\UserController::class,'store'])->name('users.store')->middleware('permission:user_store');
    Route::get('/{id}',[\App\Http\Controllers\UserController::class,'show'])->name('users.show')->middleware('permission:user_show');
    Route::get('/edit/{id}',[\App\Http\Controllers\UserController::class,'edit'])->name('users.edit')->middleware('permission:user_edit');
    Route::patch('/update/{id}',[\App\Http\Controllers\UserController::class,'update'])->name('users.update')->middleware('permission:user_update');
    Route::delete('/delete/{id}',[\App\Http\Controllers\UserController::class,'delete'])->name('users.destroy')->middleware('permission:user_delete');
});
